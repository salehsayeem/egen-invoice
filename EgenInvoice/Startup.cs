﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EgenInvoice.Startup))]
namespace EgenInvoice
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
